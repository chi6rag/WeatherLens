package com.chiragaggarwal.weatherlens;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.chiragaggarwal.weatherlens.dependencies.OpenWeatherMapTestEnvironment;
import com.chiragaggarwal.weatherlens.testhelper.OpenWeatherMapMockWebServerRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.io.IOException;

import retrofit2.Retrofit;

import static com.chiragaggarwal.weatherlens.testhelper.AssetUtils.readAsset;
import static com.chiragaggarwal.weatherlens.testhelper.RequestMatchers.httpMethodIs;
import static com.chiragaggarwal.weatherlens.testhelper.RequestMatchers.pathStartsWith;
import static org.hamcrest.CoreMatchers.allOf;

@RunWith(AndroidJUnit4.class)
public class WeatherForecastClientTest {
    @Rule
    public OpenWeatherMapTestEnvironment openWeatherMapTestEnvironment = new OpenWeatherMapTestEnvironment();

    @Rule
    public OpenWeatherMapMockWebServerRule openWeatherMapMockWebServerRule = new OpenWeatherMapMockWebServerRule();
    private Retrofit retrofit;

    @Before
    public void setup() {
        retrofit = ((WeatherLensApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getWeatherLensDependencies().getRetrofit();
    }

    @Test
    public void testThatWeatherForecastClientDeliversAFailureResponseWhenQueriedForAnInvalidCity() throws IOException {
        String failureForecastResponse = readAsset("forecast_response_failure_invalid_city.json");
        openWeatherMapMockWebServerRule.mock(
                allOf(
                        httpMethodIs("GET"),
                        pathStartsWith("/data/2.5/forecast/daily?mode=json&units=metric&cnt=5")
                ),
                200, failureForecastResponse, null
        );
        ForecastNetworkService forecastNetworkService = retrofit.create(ForecastNetworkService.class);
        Callback weatherForecastCallback = Mockito.mock(Callback.class);
        WeatherForecastClient weatherForecastClient = new WeatherForecastClient(forecastNetworkService);
        weatherForecastClient.getWeatherForecast(weatherForecastCallback);
        Mockito.verify(weatherForecastCallback).onFailure();
    }
}
