package com.chiragaggarwal.weatherlens;

import org.junit.Test;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class WeatherForecastTest {
    @Test
    public void testThatWeatherForecastIsNotEqualToNull() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 20.5f, 24.81f, WeatherCondition.CLEAR);
        assertFalse(weatherForecast.equals(null));
    }

    @Test
    public void testThatWeatherForecastIsItself() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 20.5f, 24.81f, WeatherCondition.CLEAR);
        assertTrue(weatherForecast.equals(weatherForecast));
    }

    @Test
    public void testThatAWeatherForecastIsEqualToAnotherWeatherForecastWithSameStates() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 20.5f, 24.81f, WeatherCondition.CLEAR);
        WeatherForecast otherWeatherForecast = new WeatherForecast(date, 20.5f, 24.81f, WeatherCondition.CLEAR);
        assertTrue(weatherForecast.equals(otherWeatherForecast));
    }

    @Test
    public void testThatAWeatherForecastIsNotEqualToAnotherWeatherForecastWithDifferentStates() {
        Date firstWeatherForecastDate = new Date();
        Date secondWeatherForecastDate = new Date();
        secondWeatherForecastDate.setTime(System.currentTimeMillis() - 10000);
        WeatherForecast weatherForecast = new WeatherForecast(firstWeatherForecastDate, 20.5f, 24.81f, WeatherCondition.CLEAR);
        WeatherForecast otherWeatherForecast = new WeatherForecast(secondWeatherForecastDate, 21.5f, 29.31f, WeatherCondition.EXTREME);
        assertFalse(weatherForecast.equals(otherWeatherForecast));
    }

    @Test
    public void testThatWeatherForecastIsNotEqualToAnythingOtherThanAWeatherForecast() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 20.5f, 24.81f, WeatherCondition.CLEAR);
        assertFalse(weatherForecast.equals(new Object()));
    }

    @Test
    public void testThatTwoEqualWeatherForecastsHaveEqualHashCodes() {
        Date firstWeatherForecastDate = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(firstWeatherForecastDate, 20.5f, 24.81f, WeatherCondition.CLEAR);
        WeatherForecast thatWeatherForecast = new WeatherForecast(firstWeatherForecastDate, 20.5f, 24.81f, WeatherCondition.CLEAR);
        assertEquals(weatherForecast.hashCode(), thatWeatherForecast.hashCode());
    }
}
