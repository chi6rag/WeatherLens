package com.chiragaggarwal.weatherlens.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;

import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Request.class, HttpUrl.class, HttpUrl.Builder.class, Interceptor.Chain.class})
public class OpenWeatherMapRequestInterceptorTest {
    @Test
    public void testThatItAppendsAppIdToEveryRequest() throws IOException {
        String someRandomAppId = "D53HC4fw635tYASq6Q6E7S58axG65fuP5P5G9GteM339B";
        OpenWeatherMapRequestInterceptor openWeatherMapRequestInterceptor = new OpenWeatherMapRequestInterceptor(someRandomAppId);
        Interceptor.Chain chain = mock(Interceptor.Chain.class);
        Request request = mock(Request.class);
        Request.Builder requestBuilder = mock(Request.Builder.class);
        String formerHttpUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?q=vka&mode=json&units=metric&cnt=5";
        String finalHttpUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?q=vka&mode=json&units=metric&cnt=5&appid=D53HC4fw635tYASq6Q6E7S58axG65fuP5P5G9GteM339B";
        when(chain.request()).thenReturn(request);
        when(request.url()).thenReturn(HttpUrl.parse(formerHttpUrl));
        when(requestBuilder.build()).thenReturn(request);
        when(request.newBuilder()).thenReturn(requestBuilder);
        when(requestBuilder.url(Matchers.<HttpUrl>any())).thenReturn(requestBuilder);

        openWeatherMapRequestInterceptor.intercept(chain);

        verify(requestBuilder).url(HttpUrl.parse(finalHttpUrl));
    }
}
