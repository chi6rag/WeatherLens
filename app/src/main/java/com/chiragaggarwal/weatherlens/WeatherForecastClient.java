package com.chiragaggarwal.weatherlens;

public class WeatherForecastClient {
    private ForecastNetworkService forecastNetworkService;

    public WeatherForecastClient(ForecastNetworkService forecastNetworkService) {
        this.forecastNetworkService = forecastNetworkService;
    }

    public void getWeatherForecast(Callback weatherForecastCallback) {
        weatherForecastCallback.onFailure();
    }
}
