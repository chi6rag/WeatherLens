package com.chiragaggarwal.weatherlens;

public interface Callback<T> {
    void onSuccess(T t);

    void onFailure();
}
