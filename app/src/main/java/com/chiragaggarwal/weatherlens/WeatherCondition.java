package com.chiragaggarwal.weatherlens;

import android.support.annotation.StringDef;

@StringDef({
        WeatherCondition.ADDITIONAL,
        WeatherCondition.EXTREME,
        WeatherCondition.CLOUDS,
        WeatherCondition.CLEAR,
        WeatherCondition.ATMOSPHERE,
        WeatherCondition.SNOW,
        WeatherCondition.RAIN,
        WeatherCondition.DRIZZLE,
        WeatherCondition.THUNDERSTORM,
})
public @interface WeatherCondition {
    String ADDITIONAL = "Additional";
    String EXTREME = "Extreme";
    String CLOUDS = "Clouds";
    String CLEAR = "Clear";
    String ATMOSPHERE = "Atmosphere";
    String SNOW = "Snow";
    String RAIN = "Rain";
    String DRIZZLE = "Drizzle";
    String THUNDERSTORM = "Thunderstorm";
}
