package com.chiragaggarwal.weatherlens;

import java.util.Date;

// WeatherForecast is a prediction of the state of the atmospehre

public class WeatherForecast {
    private Date date;
    private float minimumTemperature;
    private float maximumTemperature;
    private String condition;

    public WeatherForecast(Date date, float minimumTemperature, float maximumTemperature, @WeatherCondition String condition) {
        this.date = date;
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
        this.condition = condition;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || !(obj instanceof WeatherForecast)) return false;
        WeatherForecast thatWeatherForecast = (WeatherForecast) obj;
        return this.date.equals(thatWeatherForecast.date) &&
                this.maximumTemperature == thatWeatherForecast.maximumTemperature &&
                this.minimumTemperature == thatWeatherForecast.minimumTemperature &&
                this.condition.equals(thatWeatherForecast.condition);
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (minimumTemperature != +0.0f ? Float.floatToIntBits(minimumTemperature) : 0);
        result = 31 * result + (maximumTemperature != +0.0f ? Float.floatToIntBits(maximumTemperature) : 0);
        result = 31 * result + (condition != null ? condition.hashCode() : 0);
        return result;
    }
}
