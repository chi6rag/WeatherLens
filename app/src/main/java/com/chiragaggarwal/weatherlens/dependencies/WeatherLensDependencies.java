package com.chiragaggarwal.weatherlens.dependencies;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ConfigurationModule.class})
public interface WeatherLensDependencies {
    Retrofit getRetrofit();
}
