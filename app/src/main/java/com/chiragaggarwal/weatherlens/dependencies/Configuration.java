package com.chiragaggarwal.weatherlens.dependencies;

public class Configuration {
    private String openWeatherMapBaseUrl;

    public Configuration(String openWeatherMapBaseUrl) {
        this.openWeatherMapBaseUrl = openWeatherMapBaseUrl;
    }

    public String getOpenWeatherMapBaseUrl() {
        return openWeatherMapBaseUrl;
    }
}
